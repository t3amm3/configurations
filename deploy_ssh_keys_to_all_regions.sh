#!/bin/bash

regions=$(aws ec2 describe-regions --output text|awk '{print $3}'|xargs)

for region in $regions; do
	aws ec2 import-key-pair --key-name global-ssh-key --public-key-material file://~/.ssh/global-ssh-key.pub --region $region
done

